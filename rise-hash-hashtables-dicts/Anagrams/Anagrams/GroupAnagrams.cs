﻿using System.Diagnostics.Tracing;
using System.Reflection;

namespace Anagrams
{
    public class Anagrams
    {
        static void Main(string[] args)
        {

            Dictionary<string, List<string>> anagrams = new Dictionary<string, List<string>>();

            Console.WriteLine("Please enter the words");
            string[] input = Console.ReadLine().Split(", ");

            generateAnagrams(input, anagrams);

            foreach (KeyValuePair<string, List<string>> pair in anagrams)
            {
                Console.WriteLine(string.Join(" ", pair.Value));
            }

        }
        
        /*public static bool IsAnagram(string wordOne, string wordTwo)
        {
            char[] wordOneChars = wordOne.ToLower().ToCharArray();
            char[] wordTwoChars = wordTwo.ToLower().ToCharArray();

            Array.Sort(wordOneChars, wordTwoChars);

            return wordOneChars.ToString() == wordTwoChars.ToString();
        }
        */
        public static void generateAnagrams(string[] input, Dictionary<string, List<string>> anagrams) { // Така подадено Dictionary се подава копие, което се променя във фунцията, но не се променя в Main. Трябва да напишеш ключовата дума ref пред типа на аргумента
            foreach (string word in input)  // иначе алгоритъма е правилен
            {
                char[] charArray = word.ToCharArray();
                Array.Sort(charArray);

                string processedWord = new string(charArray);

                if (!anagrams.ContainsKey(processedWord))
                {
                    anagrams.Add(processedWord, new List<string> { word });
                }
                else
                {
                    anagrams[processedWord].Add(word);
                }
            }
        }
    }
}