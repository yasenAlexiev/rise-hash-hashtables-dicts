﻿namespace ArrayIntersection
{
    public class ArrayIntersection
    {
        static void Main(string[] args)
        {
            string[] array = { "one", "two", "three" };
            string[] arrayTwo = { "one", "two", "three", "five", "eight" };

           /* string[] result = array.Intersect(arrayTwo).ToArray();
            //foreach (string item in result)
            {
                Console.WriteLine(item);
            }*/

            Dictionary<string, int> intersection = new Dictionary<string, int>();

            foreach (string item in array) 
            {
                if (arrayTwo.Contains(item)) // Идеята беше да сложиш единия масив в HashSet и да търсиш думите от другия в този HashSet с Contains за константно време O(1)
                {
                    intersection["item"] = 1; // Така направено, не виждам много смисъл изобщо да имаш Dictionary<string, int> intersection
                    Console.WriteLine($"Element '{item}' is in both arrays.");
                }
            }

        }
    }
}