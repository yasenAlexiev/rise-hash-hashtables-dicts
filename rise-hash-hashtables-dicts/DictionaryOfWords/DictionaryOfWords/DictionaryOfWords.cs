﻿namespace DictionaryOfWords
{
    public class DictionaryOfWords
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Please enter the correct word dictionary");
            string[] correctDict = Console.ReadLine().Split(" ");

            Console.WriteLine("Please enter a string for a word-by-word type check:");            
            string[] inputWords = Console.ReadLine().Split(" ");            
            
            HashSet<string> misspelledWords = GetMispelledfWords(correctDict, inputWords);

            if (misspelledWords != null) {
                foreach (string word in misspelledWords)
                {
                    Console.WriteLine($"The word '{word}' is not written correctly!");
                }
            } 
        }

        public static HashSet<string> GetMispelledfWords(string[] inputDict, string[] checkedWords)
        { 

            HashSet<string> mispelledWords = new HashSet<string>(); 

            foreach (string word in checkedWords) { // Сега времето на този алгоритъм е O(m*n)( O(n^2) - бавно), където m и n са броя на думите в двата масива.
                if (!inputDict.Contains(word)) // Идеята беше да сложите inputDict думите в HashSet и Contains метода да работи за O(1). 
                {                               // Ако тръсим дума от checkedWords в inputDict за константно време, алгоритъма ще стане линеен
                    mispelledWords.Add(word); // Домавянето на думата е за константно време, но търсенето ни е по-важно да е бързо
                }
            }
            
            return mispelledWords;
        }
    }
}